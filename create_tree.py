#364319003
#typeid==116680003

import csv
from tqdm import tqdm
import sys


sys.setrecursionlimit(2500)

REL_TYPE='116680003'
ID_LIST=['364319003','26864007','414025005','386637004','106112009','248982007', '362972006', '173300003', '773261007', '271903000', '312850006', '281666001', '406224006', '364319003', '26864007', '386637004', '414025005', '106112009', '386811000', '364584001', '276507005']

def get_desc_dict(filename):
	desc_dict={}
	with open(filename) as desc_file:
		desc_csv=csv.reader(desc_file, delimiter='\t')
		for line in desc_csv:
			desc_dict[line[4]]=line[7] 
	return desc_dict

def Remove(duplicate): 
	final_list = [] 
	for num in duplicate: 
		if num not in final_list: 
			final_list.append(num) 
	print("Removed {} duplicates".format(len(duplicate)-len(final_list)))
	return final_list 

def get_rel_lines(rel_list,check_id,indent,desc_dict):
	global out_string
	global counter
	global pairs
	counter=counter+1
	if counter%100==0:
		print("Passed: {} ".format(counter))
	#with open('out.csv', 'w') as csvfile:
	#out_csv=csv.writer(csvfile)
	temp_res=[x[4] for x in rel_list if x[5]==check_id]
	if len(temp_res)==0:
		return None
	else:
		for child_id in temp_res:
			if [check_id,child_id] not in pairs:
				if child_id in desc_dict:
					out_string=out_string+'\t'*indent+child_id+'_'+desc_dict[child_id]+'\n'
					pairs.append([check_id,child_id])
				else:
					out_string=out_string+'\t'*indent+child_id+'\n'
			else:
				continue
			get_rel_lines(rel_list,child_id,indent+1,desc_dict)

#Read relationship file. Put it on list.
print("Loading list...")
rel_lines=[]
with open("Relationship.txt",'r') as rel_file:
	rel_csv = csv.reader(rel_file, delimiter='\t')
	for line in rel_csv:
		rel_lines.append(line)

sorted_rel_list=[x for x in rel_lines if x[7]==REL_TYPE]
filtered_rel_list=Remove([[x[5],x[4]] for x in sorted_rel_list])

#rel_dict={x[5]: x[4] for x in sorted_rel_list}

#Get descriptions dict
print("Loading dict...")
desc_dict=get_desc_dict('Description.txt')

#Declare out string
out_string=''
pairs=[]
#out_string='364319003_'+desc_dict['364319003']+'\n'
counter=1
print("Starting recursive run...")
for el in tqdm(ID_LIST):
	if el in desc_dict:
		out_string=out_string+el+'_'+desc_dict[el]+'\n'
		get_rel_lines(filtered_rel_list,el,1,desc_dict)
	else:
		out_string=out_string+el+'\n'
		get_rel_lines(filtered_rel_list,el,1,desc_dict)

out_lines=out_string.split('\n')
with open('out.csv','w') as out_file:
	csvwriter=csv.writer(out_file)
	for line in out_lines:
		csvwriter.writerow(line.split('\t'))


